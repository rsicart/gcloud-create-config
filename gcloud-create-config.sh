#!/bin/bash

function usage {
    echo "./$0 project region account cluster"
    echo "    project: GCP project name"
    echo "    region: region name in europe"
    echo "    account: email account"
    echo "    cluster: gke cluster name"
}

# check fixed number of args
if [[ "$#" != "4" ]]; then
    usage
    exit 1
fi

# check empty
if [[ -z "$1" ]] || [[ -z "$2" ]] || [[ -z "$3" ]] || [[ -z "$4" ]]; then
    usage
    exit 1
fi

# fill vars
GCP_PROJECT="$1"
GCP_REGION="$2"
GCP_ZONE="${GCP_REGION}-b"
GCP_ACCOUNT="$3"
GCP_CLUSTER="$4"

# validate values
VALIDATE_REGION=$(echo $GCP_REGION | grep 'europe')
if [[ "$?" != "0" ]]; then
    echo "Invalid region. Use an european region, e.g. europe-west1"
    usage
    exit 1
fi

VALIDATE_ACCOUNT=$(echo $GCP_ACCOUNT | grep '@fr.clara.net')
if [[ "$?" != "0" ]]; then
    echo "Invalid account. Use a Claranet France email account, e.g. john.doe@fr.clara.net"
    usage
    exit 1
fi

VALIDATE_CONFIGURATION=$(gcloud config configurations activate "$GCP_PROJECT")
if [[ "$?" != "0" ]]; then
    # configuration doesn't exist, create it
    gcloud config configurations create "${GCP_PROJECT}"
    gcloud config set project "${GCP_PROJECT}"
    gcloud config set account "${GCP_ACCOUNT}"
    gcloud auth login
    gcloud config set compute/region "${GCP_REGION}"
    gcloud config set compute/zone "${GCP_ZONE}"
fi

VALIDATE_CLUSTER=$(gcloud container clusters list | awk '{print $1}' | grep "$GCP_CLUSTER")
if [[ "$?" != "0" ]]; then
    echo "Invalid cluster, ignoring cluster configuration."
else
    gcloud container clusters get-credentials $GCP_CLUSTER --region=$GCP_REGION
    # get current context user
    K8S_CONTEXT_USER=$(kubectl config view -o jsonpath="{.contexts[?(@.name == '$(kubectl config current-context)')].context.user}")
    # get current context cluster
    K8S_CONTEXT_CLUSTER=$(kubectl config view -o jsonpath="{.contexts[?(@.name == '$(kubectl config current-context)')].context.cluster}")
    if [[ -z "$K8S_CONTEXT_USER" ]] || [[ -z "$K8S_CONTEXT_CLUSTER" ]]; then
        echo "Can't find k8s context user or cluster, ignoring k8s context creation."
    else
        kubectl config set-context $GCP_CLUSTER --user="$K8S_CONTEXT_USER" --cluster="$K8S_CONTEXT_CLUSTER"
    fi
fi
